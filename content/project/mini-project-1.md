+++
title = "Mini project 1"
date = 2024-01-30
category = "Programming"
tags = ["rust"]
+++

Continuous Delivery of Personal Website


## Project Description

Requirements:
- Website built with Zola, Hugo, Gatsby, Next.js or equivalent
- GitLab workflow to build and deploy site on push
- Hosted on Vercel, Netlify, AWS Amplify, AWS S3, or others.


## Project Outcomes

This website is the project outcome.

