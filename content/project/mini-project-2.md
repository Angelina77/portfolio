+++
title = "Mini project 2"
date = 2024-02-27
category = "Programming"
tags = ["rust"]
+++

Continuous Delivery of Rust Microservice


## Project Description

- Simple REST API/web service in Rust
- Dockerfile to containerize service
- CI/CD pipeline files


## Project Outcomes

gitlab link here

## Conclusion

Your conclusions about the project or what you have learned from it.

