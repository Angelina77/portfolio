+++
title = "Mini project 3"
date = 2024-03-30
category = "Programming"
tags = ["rust"]
+++

Rust Vector Database or Polar DataFrame API


## Project Introduction

- High-performance vector database or Polars Dataframe invoked in Rust
- Data ingestion and query functionality
- Benchmarks API Performance


## Project Outcomes

gitlab link here

## Conclusion

Your conclusions about the project or what you have learned from it.

